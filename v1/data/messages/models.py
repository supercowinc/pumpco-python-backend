from __future__ import unicode_literals

from django.db import models
from v1.data.user_profile.models import UserProfile

# Create your models here.


class Messages(models.Model):
    MESSAGE_CHOICES = (
        ('work_order', 'work_order'),
        ('log', 'log'),
    )
    message_type = models.CharField(max_length=40, choices=MESSAGE_CHOICES)
    related_id = models.IntegerField(null=True, blank=True)
    date_posted = models.DateField(auto_now_add=True)
    sender = models.ForeignKey(UserProfile, unique=False, null=True)
    message = models.CharField(max_length=500, blank=True, null=True)
