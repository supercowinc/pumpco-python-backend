from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.messages.views\
    import MessageViewSet,\
    LogMessageViewSet,\
    WorkOrderMessageViewSet

router = BulkRouter()
router.register(r'messages', MessageViewSet)

urlpatterns = [
    url('^messages/log/(?P<related_id>.+)/$', LogMessageViewSet.as_view()),
    url('^messages/work_order/(?P<related_id>.+)/$',
        WorkOrderMessageViewSet.as_view())
]

urlpatterns += router.urls
