from models import Messages
from rest_framework.serializers import ModelSerializer, SerializerMethodField


class DetailMessageSerializer(ModelSerializer):
    sender = SerializerMethodField()

    def get_sender(self, obj):
        if obj.sender:
            return obj.sender.name

    class Meta(object):
        model = Messages
        fields = '__all__'


class MessageSerializer(ModelSerializer):

    class Meta(object):
        model = Messages
        fields = '__all__'
