from models import Messages
from serializers import MessageSerializer, DetailMessageSerializer
from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from rest_framework.generics import ListAPIView

# Create your views here.


class MessageViewSet(BulkModelViewSet):
    serializer_class = MessageSerializer
    queryset = Messages.objects.all()
    permission_classes = (AllowAny,)
    authentication_class = ()


class LogMessageViewSet(ListAPIView):
    serializer_class = DetailMessageSerializer
    permission_classes = (AllowAny,)
    authentication_class = ()

    def get_queryset(self):
        related_id = self.kwargs['related_id']
        return Messages.objects.filter(
            message_type='log',
            related_id=related_id
        )


class WorkOrderMessageViewSet(ListAPIView):
    serializer_class = DetailMessageSerializer
    permission_classes = (AllowAny,)
    authentication_class = ()

    def get_queryset(self):
        related_id = self.kwargs['related_id']
        return Messages.objects.filter(
            message_type='work_order',
            related_id=related_id
        )
