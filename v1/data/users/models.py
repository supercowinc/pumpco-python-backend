from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, UserManager

# Create your models here.


class User(AbstractBaseUser):

    ROLE_CHOICES = (
        ('operator', 'Operator'),
        ('mechanic', 'Mechanic'),
        ('staff', 'Staff'),
        ('admin', 'Admin'),
    )
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=200)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    role = models.CharField(max_length=100, choices=ROLE_CHOICES)

    REQUIRED_FIELDS = ['password']
    USERNAME_FIELD = 'email'

    objects = UserManager()
