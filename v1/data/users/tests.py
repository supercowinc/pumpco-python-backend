from models import User
from rest_framework.test import APITestCase
from rest_framework import status

# Create your tests here.

class AuthTest(APITestCase):
    def setUp(self):
        url = "http://localhost:8000/api/v1/auth/register/"
        data = {"email": "jesse@jesse.com", "password":"password", "role": "operator"}
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_login(self):
        """
        Ensure you can login
        """
        url = "http://localhost:8000/api/v1/auth/login/"
        data = {"email": "jesse@jesse.com", "password": "password"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.data)
