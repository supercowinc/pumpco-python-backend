from rest_framework import serializers
from django.core.validators import EmailValidator
from django.shortcuts import get_object_or_404
from django.core import exceptions
from django.contrib.auth import authenticate
from models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = '__all__'
        read_only_field = ['id']
        extra_kwargs = {'password': {'write_only': True},
                        'is_active': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance


class AuthCustomTokenSerializer(serializers.Serializer):

    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            if EmailValidator(email):
                user_request = get_object_or_404(
                    User,
                    email=email,
                )

                email = user_request.email

            user = authenticate(email=email, password=password)

            if user:
                if not user.is_active:
                    msg = ('User account is disabled.')
                    raise exceptions.ValidationError(msg)
            else:
                msg = ('Unable to log in with provided credentials.')
                raise exceptions.ValidationError(msg)
        else:
            msg = ('Must include "email" and "password"')
            raise exceptions.ValidationError(msg)

        attrs['user'] = user
        return attrs
