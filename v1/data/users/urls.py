from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.users.views import RegisterView, obtain_auth_token, UserViewSet

router = BulkRouter()
router.register('auth/user', UserViewSet)
router.register('auth/register', RegisterView)

urlpatterns = [
    # url(r'^auth/register/?$', RegisterView.as_view(), name='register'),
    url(r'^auth/login/', obtain_auth_token)
]

urlpatterns += router.urls
