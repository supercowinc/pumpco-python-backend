from serializers import UserSerializer, AuthCustomTokenSerializer
from rest_framework_bulk import BulkModelViewSet
from models import User
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny
from rest_framework.authtoken.models import Token
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework import parsers, renderers

# Create your views here.


class RegisterView(BulkModelViewSet):
    """
    Register View
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )
    authentication_classes = ()


class ObtainAuthToken(APIView):
    """
    Login endpoint
    """
    permission_classes = [AllowAny]
    parser_classes = (
        parsers.FormParser,
        parsers.MultiPartParser,
        parsers.JSONParser,
    )
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthCustomTokenSerializer
    authentication_classes = ()

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'user_email': user.email,
            'user_id': user.id,
            'user_role': user.role
        })


obtain_auth_token = ObtainAuthToken.as_view()
