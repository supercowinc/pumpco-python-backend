from __future__ import unicode_literals

from django.db import models
from v1.data.users.models import User

# Create your models here.


class Units(models.Model):
    UNIT_CHOICES = (
        ('truck', 'truck'),
        ('trailer', 'trailer'),
        ('equipment', 'equipment'),
    )
    UNIT_STATUS_CHOICES = (
        ('Down', 'down'),
        ('Needs Attention', 'needs attention'),
        ('Information Only', 'information only'),
        ('Prep for Disposal', 'prep for disposal')
    )
    unit_status = models.CharField(
        max_length=100, choices=UNIT_STATUS_CHOICES, null=True, blank=True)
    unit_type = models.CharField(max_length=100, blank=True, null=True)
    operator = models.ForeignKey(User, blank=True, null=True)
    number = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    vin_sn = models.CharField(max_length=255, blank=True, null=True)
    plate = models.CharField(max_length=255, blank=True, null=True)
    registration_date = models.CharField(max_length=255, blank=True, null=True)
    company = models.CharField(max_length=255, blank=True, null=True)
