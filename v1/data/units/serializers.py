from rest_framework import serializers
from models import Units


class UnitSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = Units
        fields = '__all__'


class MobileUnitSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = Units
        fields = (
            'id',
            'unit_type',
            'number',
        )
