from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.units.views import\
    UnitsViewSet, UnitTypeViewSet, SmallMobileUnitGrab

router = BulkRouter()
router.register(r'units', UnitsViewSet)

urlpatterns = [
    url('^unit_type/(?P<unit_type>.+)/$', UnitTypeViewSet.as_view()),
    url('^units/new/(?P<count>.+)/$', SmallMobileUnitGrab.as_view())
]

urlpatterns += router.urls
