from rest_framework_bulk import BulkModelViewSet
from serializers import UnitSerializer, MobileUnitSerializer
from models import Units
from rest_framework.permissions import AllowAny
from rest_framework import generics
from rest_framework.decorators import list_route
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

# Create your views here.


class UnitsViewSet(BulkModelViewSet):
    serializer_class = UnitSerializer
    queryset = Units.objects.all()#\
        #.order_by('number')
    permission_classes = (AllowAny,)
    authentication_classes = ()
    lookup_field = 'number'

    @method_decorator(cache_page(60 * 600))
    def dispatch(self, *args, **kwargs):
        return super(UnitsViewSet, self).dispatch(*args, **kwargs)

    def get_serializer_class(self):
        if self.action == 'mobile':
            return MobileUnitSerializer
        else:
            return UnitSerializer

    @list_route(methods=['GET'])
    def count(self, request):
        return Response(self.get_queryset().count())

    @list_route(methods=['GET'])
    def mobile(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class UnitTypeViewSet(generics.ListAPIView):
    serializer_class = UnitSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get_queryset(self):
        unit_type = self.kwargs['unit_type']
        return Units.objects.filter(unit_type=unit_type)


class SmallMobileUnitGrab(generics.ListAPIView):
    serializer_class = MobileUnitSerializer
    permission_classes = (AllowAny, )
    authentication_classes = ()

    def get_queryset(self):
        count = self.kwargs['count']
        return Units.objects.filter(id__gt=count)
