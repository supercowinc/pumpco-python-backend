from __future__ import unicode_literals

from django.db import models
from v1.data.users.models import User
from v1.data.jobs.models import Jobs

# Create your models here.


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, to_field='id')
    name = models.CharField(max_length=150, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    job = models.ForeignKey(Jobs, blank=True, null=True)
    employee_number = models.CharField(max_length=50, blank=True, null=True)
