from rest_framework_bulk.routes import BulkRouter
from v1.data.user_profile.views\
    import\
    UserProfileDetailViewSet,\
    UserProfileViewSet,\
    OperatorUserProfileViewSet,\
    MechanicUserProfileViewSet

router = BulkRouter()
router.register(r'users', UserProfileDetailViewSet)
router.register(r'user_profile', UserProfileViewSet)
router.register(r'user/operator', OperatorUserProfileViewSet)
router.register(r'user/mechanic', MechanicUserProfileViewSet)

urlpatterns = []

urlpatterns += router.urls
