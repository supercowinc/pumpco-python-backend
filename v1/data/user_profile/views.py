from rest_framework_bulk import BulkModelViewSet
from models import UserProfile
from serializers\
    import UserProfileSerializer,\
    UserDetailProfileSerializer, MobileUserProfileSerializer
from rest_framework.permissions import AllowAny
from rest_framework.decorators import list_route
from rest_framework.response import Response

# Create your views here.


class UserProfileDetailViewSet(BulkModelViewSet):
    serializer_class = UserDetailProfileSerializer
    queryset = UserProfile.objects.all()\
        .select_related('user').select_related('job')
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get_serializer_class(self):
        if self.action == 'mobile':
            return MobileUserProfileSerializer
        else:
            return UserDetailProfileSerializer

    @list_route(methods=['GET'])
    def mobile(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class UserProfileViewSet(BulkModelViewSet):
    serializer_class = UserProfileSerializer
    queryset = UserProfile.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()


class OperatorUserProfileViewSet(BulkModelViewSet):
    serializer_class = UserDetailProfileSerializer
    queryset = UserProfile.objects.filter(user__role='operator')\
        .order_by('-name').reverse()
    permission_classes = (AllowAny, )
    authentication_classes = ()


class MechanicUserProfileViewSet(BulkModelViewSet):
    serializer_class = UserDetailProfileSerializer
    queryset = UserProfile.objects.filter(user__role='mechanic')\
        .order_by('-name').reverse()
    permission_classes = (AllowAny, )
    authentication_classes = ()
