from rest_framework.serializers import ModelSerializer, SerializerMethodField
from models import UserProfile
from v1.data.users.serializers import UserSerializer
from v1.data.jobs.serializers import JobSerializer


class UserDetailProfileSerializer(ModelSerializer):
    user = UserSerializer(many=False, read_only=True)
    job = JobSerializer(many=False, read_only=True)

    class Meta(object):
        model = UserProfile
        fields = '__all__'


class UserProfileSerializer(ModelSerializer):
    '''
    user = SerializerMethodField()
    job = SerializerMethodField()

    def get_user(self, obj):
        if obj.user:
            return obj.user.email

    def get_job(self, obj):
        if obj.job:
            return obj.job.name
    '''

    class Meta(object):
        model = UserProfile
        fields = '__all__'


class MobileUserProfileSerializer(ModelSerializer):
    user = SerializerMethodField()
    job = SerializerMethodField()

    def get_user(self, obj):
        if obj.user:
            return obj.user.id

    def get_job(self, obj):
        if obj.job:
            return obj.job.id

    class Meta:
        model = UserProfile
        fields = ['user', 'job']
