from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.work_order.views\
    import WorkOrderViewSet, UniqueWorkOrderViewSet, WorkOrderDetailViewSet

router = BulkRouter()
router.register(r'work_orders', WorkOrderViewSet)
router.register(r'work_order', WorkOrderDetailViewSet)

urlpatterns = [
    url('^open/(?P<unit_number>.+)/$',
        UniqueWorkOrderViewSet.as_view())
]

urlpatterns += router.urls
