from __future__ import unicode_literals

from django.db import models
from v1.data.purchase_order.models import PurchaseOrder
from v1.data.logs.models import Logs
from v1.data.users.models import User
from v1.data.vendors.models import Vendor
from v1.data.units.models import Units

# Create your models here.


class WorkOrder(models.Model):
    WORK_ORDER_STATUS_CHOICES = (
        ('Open', 'open'),
        ('Closed', 'closed'),
        ('Sent to Vendor', 'sent to vendor'),
        ('In Progress', 'in progress'),
        ('Waiting on Parts', 'waiting on parts'),
        ('Cancelled', 'cancelled'),
    )
    UNIT_STATUS_CHOICES = (
        ('Down', 'down'),
        ('Needs Attention', 'needs attention'),
        ('Information Only', 'information only'),
        ('Prep for Disposal', 'prep for disposal')
    )
    unit_status = models.CharField(
        max_length=100, choices=UNIT_STATUS_CHOICES, null=True, blank=True)
    created_at = models.DateField(auto_now_add=True)
    closed_date = models.CharField(max_length=50, blank=True, null=True)
    unit = models.ForeignKey(Units)
    po_number = models.ForeignKey(PurchaseOrder, blank=True, null=True)
    log_number = models.ManyToManyField(Logs, blank=True)
    status = models.CharField(
        max_length=100, choices=WORK_ORDER_STATUS_CHOICES)
    mechanic = models.ForeignKey(
        User, blank=True, null=True, related_name="mechanic")
    operator = models.ForeignKey(
        User, blank=True, null=True, related_name="operator")
    deficient_properties = models.CharField(
        max_length=450, blank=True, null=True)
    properties_fixed = models.CharField(max_length=450, blank=True, null=True)
    vendor = models.ForeignKey(Vendor, blank=True, null=True)
    meter = models.DecimalField(
        max_digits=10, decimal_places=0, blank=True, null=True)
