from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework import status
from v1.data.units.models import Units

# Using the standard RequestFactory API to create a form POST request
class WorkOrderTest(APITestCase):
    def setUp(self):
        url = "http://localhost:8000/api/v1/work_orders/"
        Units.objects.create(
            "number" : 114,
        )
        data = {
            "status": "Open",
            "deficient_properties": "backup_lights",
            "properties_fixed": "lugs",
            "meter": 123456,
            "unit_number": 114,
            "po_number": '',
            "mechanic": '',
            "operator": '',
            "vendor": '',
            "log_number": []
        }
        response = self.client.post(url, data, format="json")
        print response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_work_orders(self):
        url = "http://localhost:8000/api/v1/work_orders/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
