from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from models import WorkOrder
from serializers import WorkOrderSerializer, WorkOrderDetailSerializer
from rest_framework.generics import ListAPIView
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED

# Create your views here.


class WorkOrderViewSet(BulkModelViewSet):
    serializer_class = WorkOrderSerializer
    queryset = WorkOrder.objects.all()
    permission_classes = (AllowAny,)
    authentication_classes = ()

    @list_route(methods=['GET'])
    def count(self, request):
        return Response(self.get_queryset().count())

    @list_route(methods=['GET'])
    def open(self, request):
        serializer = WorkOrderSerializer(WorkOrder.objects.filter(
            status='open'), many=True)
        return Response(serializer.data)


class UniqueWorkOrderViewSet(ListAPIView):
    serializer_class = WorkOrderDetailSerializer
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get_queryset(self):
        unit_number = self.kwargs['unit_number']
        return WorkOrder.objects.filter(unit=unit_number, status='open')


class WorkOrderDetailViewSet(BulkModelViewSet):
    serializer_class = WorkOrderDetailSerializer
    permission_classes = (AllowAny, )
    authentication_classes = ()
    queryset = WorkOrder.objects.all()
