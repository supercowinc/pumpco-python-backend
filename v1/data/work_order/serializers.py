from rest_framework.serializers import ModelSerializer, SerializerMethodField, PrimaryKeyRelatedField
from ..logs.models import Logs
from models import WorkOrder


class WorkOrderDetailSerializer(ModelSerializer):
    unit = SerializerMethodField()

    def get_unit(self, obj):
        return obj.unit.number

    class Meta(object):
        model = WorkOrder
        fields = '__all__'


class WorkOrderSerializer(ModelSerializer):

    class Meta(object):
        model = WorkOrder
        fields = '__all__'
