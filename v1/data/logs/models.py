from __future__ import unicode_literals
from django.db import models
from v1.data.units.models import Units
from v1.data.users.models import User
from v1.data.jobs.models import Jobs

# Create your models here.


class Logs(models.Model):
    LOG_TYPE_CHOICES = (
        ('pre-trip', 'pre-trip'),
        ('post-trip', 'post-trip'),
    )
    created_at = models.DateTimeField(auto_now_add=True)
    log_type = models.CharField(
        max_length=30, choices=LOG_TYPE_CHOICES, null=True, blank=True)
    posted_by = models.ForeignKey(User, related_name='posted_by')
    operator = models.ForeignKey(User, related_name='operator')
    unit = models.ForeignKey(Units)
    job = models.ForeignKey(Jobs)
    date = models.CharField(
        max_length=200, blank=True, null=True)
    meter = models.DecimalField(
        max_digits=10, decimal_places=2, blank=True, null=True)
    # To replace columns of properties
    deficiency = models.NullBooleanField()
    deficient_properties = models.CharField(
        max_length=450, blank=True, null=True)  # Got rid of bitfield
    other_description = models.CharField(
        max_length=300, blank=True, null=True)
