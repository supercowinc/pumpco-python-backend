from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from serializers import LogSerializer, LogDetailSerializer, LogsInfoSerializer
from ..work_order.serializers import WorkOrderSerializer
from models import Logs
from rest_framework_bulk import BulkModelViewSet
from rest_framework.decorators import list_route
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED
from ..work_order.models import WorkOrder
from ..units.models import Units
from ..messages.models import Messages
from ..user_profile.models import UserProfile
from ..users.models import User
from django.core.exceptions import ObjectDoesNotExist
# Create your views here.


class LogsViewSet(ModelViewSet):
    serializer_class = LogSerializer
    queryset = Logs.objects.all()
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def get_serializer_class(self):
        if self.action == 'mobile':
            return LogDetailSerializer
        elif self.action == 'everything':
            return LogsInfoSerializer
        else:
            return LogSerializer

    @list_route(methods=['GET'])
    def mobile(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['GET'])
    def everything(self, request):
        queryset = Logs.objects.all()\
            .select_related('unit').select_related('job')
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


'''
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        deficiencies = serializer.data['deficient_properties']
        try:
            user = get_object_or_404(User, id=serializer.data['operator'])
        except ObjectDoesNotExist:
            print "User not found"
        try:
            user_profile = get_object_or_404(
                UserProfile, id=serializer.data['posted_by'])
        except ObjectDoesNotExist:
            print "UserProfile not found"
        try:
            unit = get_object_or_404(Units, id=serializer.data['unit'])
        except ObjectDoesNotExist:
            print "Unit not found"
        if deficiencies != '':
            log_array = deficiencies.split(', ')
            message = deficiencies + ' was/were reported as deficient'
            Messages.objects.create(
                message_type='log',
                message=message,
                related_id=serializer.data['id'],
                sender=user_profile
            )
            try:
                work_order = WorkOrder.objects.get(status='Open', unit=unit.number)
                work_order_array = work_order.deficient_properties.split(
                    ', ')
                dupes = set(log_array).intersection(work_order_array)
                dupe_result = " ".join(str(x) for x in dupes)
                for el in log_array:
                    if el == dupe_result:
                        log_array.remove(dupe_result)
                work_order_array.extend(log_array)
                work_order.deficient_properties = ", ".join(
                    work_order_array)
                work_order.save()
            except WorkOrder.DoesNotExist:
                print "Doesn't exist"
                WorkOrder.objects.create(
                    unit=unit,
                    deficient_properties=serializer.data[
                        'deficient_properties'],
                    meter=serializer.data['meter'],
                    operator=user,
                    status='Open',
                    properties_fixed='',
                )
        return Response(serializer.data, status=HTTP_201_CREATED, headers=headers)
'''


class LogViewSet(BulkModelViewSet):
    serializer_class = LogDetailSerializer
    queryset = Logs.objects.all()
    permission_classes = (AllowAny,)
    authentication_classes = ()

    def retrieve(self, request, pk=None):
        queryset = Logs.objects.all()
        log = get_object_or_404(queryset, pk=pk)
        serializer = LogDetailSerializer(log)
        return Response(serializer.data)
