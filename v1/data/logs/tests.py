from rest_framework.test import APITestCase
from rest_framework import status

# Create your tests here.


class LogTest(APITestCase):
    def setUp(self):
        url = "http://localhost:8000/api/v1/logs/"
        data = {"user": "", "date": "08/05/1987", "meter": 123456,
                "deficient_properties": "brake lights", "deficiency": True,
                "description": "brake lights are out", "unit_id": None}
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_logs(self):
        url = "http://localhost:8000/api/v1/logs/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
