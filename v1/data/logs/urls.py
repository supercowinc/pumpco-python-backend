from rest_framework_bulk.routes import BulkRouter
from v1.data.logs.views import LogViewSet, LogsViewSet
from django.conf.urls import url

router = BulkRouter()
router.register(r'log', LogViewSet)
router.register(r'logs', LogsViewSet)

urlpatterns = [
    url('^log/detail/(?P<pk>.+)/$', LogViewSet.as_view({'get': 'detail'})),
]

urlpatterns += router.urls
