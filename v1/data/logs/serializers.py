from rest_framework.serializers import ModelSerializer, SerializerMethodField
from models import Logs
from v1.data.units.serializers import UnitSerializer
from v1.data.jobs.serializers import JobSerializer


class LogSerializer(ModelSerializer):

    class Meta:
        fields = '__all__'
        model = Logs


class LogDetailSerializer(ModelSerializer):
    unit = SerializerMethodField()
    job = SerializerMethodField()
    operator = SerializerMethodField()

    def get_operator(self, obj):
        if obj.operator:
            return obj.operator.email

    def get_job(self, obj):
        if obj.job:
            return obj.job.name

    def get_unit(self, obj):
        if obj.unit:
            return obj.unit.number

    class Meta:
        fields = '__all__'
        model = Logs


class LogsInfoSerializer(ModelSerializer):
    unit = UnitSerializer(many=False, read_only=True)
    job = JobSerializer(many=False, read_only=True)

    class Meta(object):
        model = Logs
        fields = '__all__'
