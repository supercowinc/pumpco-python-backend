from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.yards.views import *

router = BulkRouter()
router.register(r'yards', YardViewSet)

urlpatterns = [ ]

urlpatterns += router.urls
