from django.shortcuts import render
from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from serializers import YardSerializer
from models import Yards

# Create your views here.


class YardViewSet(BulkModelViewSet):
    serializer_class = YardSerializer
    queryset = Yards.objects.all().select_related('address')
    permission_classes = (AllowAny,)
    authentication_classes = ()
