from rest_framework.serializers import ModelSerializer
from models import Yards


class YardSerializer(ModelSerializer):

    class Meta(object):
        model = Yards
        fields = '__all__'
