from __future__ import unicode_literals

from django.db import models
from v1.data.address.models import Address

# Create your models here.


class Yards(models.Model):
    name = models.CharField(max_length=255)
    address = models.ForeignKey(Address, blank=True, null=True)
