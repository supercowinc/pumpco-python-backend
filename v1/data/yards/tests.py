from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework import status

# Create your tests here.

class YardTest(APITestCase):
    def setUp(self):
        url = "http://localhost:8000/api/v1/yards/"
        data = {"name": "Sample Yard Name"}
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_units(self):
        url = "http://localhost:8000/api/v1/yards/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
