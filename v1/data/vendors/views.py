from django.shortcuts import render
from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from models import Vendor
from serializers import VendorSerializer

# Create your views here.

class VendorViewSet(BulkModelViewSet):
    serializer_class = VendorSerializer
    queryset = Vendor.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()
