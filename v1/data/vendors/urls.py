from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.vendors.views import VendorViewSet

router = BulkRouter()
router.register(r'vendors', VendorViewSet)

urlpatterns = [ ]

urlpatterns += router.urls
