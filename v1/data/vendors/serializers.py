from django.test import TestCase
from rest_framework.serializers import ModelSerializer
from models import Vendor

# Create your tests here.

class VendorSerializer(ModelSerializer):

    class Meta(object):
        model = Vendor
        fields = '__all__'
