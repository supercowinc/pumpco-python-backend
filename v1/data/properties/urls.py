from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.properties.views import *

router = BulkRouter()
router.register(r'unit_properties', UnitPropertiesViewSet)

urlpatterns = []

urlpatterns += router.urls
