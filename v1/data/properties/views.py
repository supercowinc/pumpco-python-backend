from django.shortcuts import render
from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from rest_framework.generics import ListAPIView
from models import UnitProperties
from serializers import UnitPropertySerializer

# Create your views here.


class UnitPropertiesViewSet(BulkModelViewSet):
    serializer_class = UnitPropertySerializer
    queryset = UnitProperties.objects.all()
    permission_classes = (AllowAny,)
    authentication_classes = ()
