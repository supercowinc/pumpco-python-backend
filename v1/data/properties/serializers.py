from rest_framework.serializers import ModelSerializer
from models import UnitProperties

class UnitPropertySerializer(ModelSerializer):

    class Meta:
        model = UnitProperties
        fields = '__all__'
