from rest_framework.serializers import ModelSerializer
from models import PurchaseOrder


class PurchaseOrderSerializer(ModelSerializer):

    class Meta(object):
        model = PurchaseOrder
        fields = '__all__'
