from __future__ import unicode_literals

from django.db import models

# Create your models here.


class PurchaseOrder(models.Model):
    number = models.CharField(max_length=150, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
