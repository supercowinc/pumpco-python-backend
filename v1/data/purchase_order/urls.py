from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.purchase_order.views import PurchaseOrderViewSet

router = BulkRouter()
router.register(r'po', PurchaseOrderViewSet)

urlpatterns = [ ]

urlpatterns += router.urls
