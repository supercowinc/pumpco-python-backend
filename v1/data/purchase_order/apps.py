from __future__ import unicode_literals

from django.apps import AppConfig


class PurchaseOrderConfig(AppConfig):
    name = 'purchase_order'
