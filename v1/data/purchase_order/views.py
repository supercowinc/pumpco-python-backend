from django.shortcuts import render
from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from models import PurchaseOrder
from serializers import PurchaseOrderSerializer

# Create your views here.

class PurchaseOrderViewSet(BulkModelViewSet):
    serializer_class = PurchaseOrderSerializer
    queryset = PurchaseOrder.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()
    
