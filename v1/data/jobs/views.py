from rest_framework_bulk import BulkModelViewSet
from rest_framework.permissions import AllowAny
from serializers import JobSerializer
from models import Jobs

# Create your views here.


class JobViewSet(BulkModelViewSet):
    serializer_class = JobSerializer
    queryset = Jobs.objects.all().select_related('yard')
    permission_classes = (AllowAny,)
    authentication_classes = ()
