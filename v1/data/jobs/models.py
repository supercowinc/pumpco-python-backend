from __future__ import unicode_literals

from django.db import models
from v1.data.yards.models import Yards

# Create your models here.


class Jobs(models.Model):
    STATUS_CHOICES = (
        ('in_progress', 'In Progress'),
        ('completed', 'Completed'),
    )
    name = models.CharField(max_length=255, blank=True, null=True)
    number = models.CharField(max_length=200, null=True, blank=True)
    status = models.CharField(max_length=100, choices=STATUS_CHOICES)
    yard = models.ForeignKey(Yards, null=True, blank=True)
