from rest_framework_bulk.routes import BulkRouter
from v1.data.jobs.views import JobViewSet

router = BulkRouter()
router.register(r'jobs', JobViewSet)

urlpatterns = []

urlpatterns += router.urls
