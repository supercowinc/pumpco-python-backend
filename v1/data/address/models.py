from __future__ import unicode_literals

from django.db import models
from localflavor.us.models\
    import PhoneNumberField, USStateField, USZipCodeField

# Create your models here.


class Address(models.Model):
    address = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=50, null=True)
    state = USStateField(null=True)
    zip_code = USZipCodeField(null=True)
    phone_number = PhoneNumberField(max_length=100, null=True)
