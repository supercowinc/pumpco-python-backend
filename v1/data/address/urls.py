from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.address.views import AddressViewSet

router = BulkRouter()
router.register(r'addresses', AddressViewSet)

urlpatterns = [ ]

urlpatterns += router.urls
