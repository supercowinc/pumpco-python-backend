from django.shortcuts import render
from rest_framework_bulk import BulkModelViewSet
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from serializers import AddressSerializer
from models import Address


# Create your views here.


class AddressViewSet(BulkModelViewSet):
    serializer_class = AddressSerializer
    queryset = Address.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()
