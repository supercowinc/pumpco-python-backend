from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from v1.data.users import urls as user_urls
from v1.data.units import urls as units_urls
from v1.data.logs import urls as logs_urls
from v1.data.jobs import urls as jobs_urls
from v1.data.yards import urls as yards_urls
from v1.data.work_order import urls as work_order_urls
from v1.data.purchase_order import urls as purchase_order_urls
from v1.data.vendors import urls as vendor_urls
from v1.data.address import urls as address_urls
from v1.data.properties import urls as property_urls
from v1.data.user_profile import urls as user_profile_urls
from v1.data.messages import urls as message_urls

router = DefaultRouter()

urlpatterns = [
    url(r'^', include(user_urls)),
    url(r'^', include(units_urls)),
    url(r'^', include(logs_urls)),
    url(r'^', include(jobs_urls)),
    url(r'^', include(yards_urls)),
    url(r'^', include(work_order_urls)),
    url(r'^', include(purchase_order_urls)),
    url(r'^', include(vendor_urls)),
    url(r'^', include(address_urls)),
    url(r'^', include(property_urls)),
    url(r'^', include(user_profile_urls)),
    url(r'^', include(message_urls))
]
